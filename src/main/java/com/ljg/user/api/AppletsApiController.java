//package com.ljg.user.api;
//
//import com.baomidou.mybatisplus.extension.api.R;
//import com.fasterxml.jackson.databind.util.BeanUtil;
//import com.ljg.common.utils.exception.CustomException;
//import com.ljg.common.utils.response.PageListData;
//import com.ljg.common.utils.response.ResponsePageResult;
//import com.ljg.common.utils.response.ResponseResult;
//import com.ljg.esserver.entities.GoodsSearch;
//import com.ljg.esserver.entities.GoodsTblES;
//import com.ljg.esserver.wrappers.GoodsDocWrapper;
//import com.ljg.goodsserver.entities.CategoryNavigationTblDTO;
//import com.ljg.goodsserver.entities.FloorTblDTO;
//import com.ljg.goodsserver.entities.GoodsTblDTO;
//import com.ljg.goodsserver.entities.SwiperTblDTO;
//import com.ljg.goodsserver.wrappers.AppletsIndexWrapper;
//import com.ljg.goodsserver.wrappers.GoodsWrapper;
//import com.ljg.user.annotation.FrequencyVerification;
//import com.ljg.user.form.GoodsSearchForm;
//import com.ljg.user.vo.FloorVO;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//@Api(value = "小程序接口")
//@RestController
//@RequestMapping()
//@RequiredArgsConstructor
//public class AppletsApiController {
//
//    @Autowired
//    private GoodsDocWrapper goodsDocWrapper;
//
//    @Autowired
//    private AppletsIndexWrapper appletsIndexWrapper;
//
//    @Autowired
//    private final GoodsWrapper goodsWrapper;
//
//
//    @FrequencyVerification
//    @ApiOperation(value = "小程序 -- 商 品集合")
//    @GetMapping("api/goodsList")
//    public ResponsePageResult<GoodsTblES> list(@RequestBody GoodsSearchForm form) throws CustomException {
//        PageListData<GoodsTblES> search = goodsDocWrapper.search(GoodsSearch.builder()
//                .goodsName(form.getGoodsName())
//                .pageindex(form.getPageindex())
//                .pagesize(form.getPagesize()).build());
//        return ResponsePageResult.success(search.getTotal(),search.getList());
//
//    }
//
//    @FrequencyVerification
//    @ApiOperation(value = "小程序 -- 商品详情")
//    @GetMapping("api/goodsDetail/{goodsId}")
//    public ResponseResult<GoodsTblDTO> goodsDetail(@PathVariable Long goodsId) throws CustomException {
//        GoodsTblDTO dto = goodsWrapper.getGoodsById(goodsId);
//        return ResponseResult.success(dto);
//
//    }
//
//    @ApiOperation(value = "小程序 -- 轮播图")
//    @GetMapping("api/swiperList")
//    public ResponseResult<SwiperTblDTO> swiperList() throws CustomException {
//        List<SwiperTblDTO> list = appletsIndexWrapper.swiperList();
//        return ResponseResult.success(list);
//
//    }
//
//    @ApiOperation(value = "小程序 -- 导航栏")
//    @GetMapping("/api/navList")
//    public ResponseResult<CategoryNavigationTblDTO> categoryNav() throws CustomException {
//        List<CategoryNavigationTblDTO> list = appletsIndexWrapper.cateGoryNavlist();
//        return ResponseResult.success(list);
//    }
//
//    @ApiOperation(value = "小程序 -- 楼层导航栏")
//    @GetMapping("/api/floorList")
//    public ResponseResult<List<FloorVO>> floorList() throws CustomException {
//
//        List<FloorTblDTO> floorTblDTOS = appletsIndexWrapper.floorList();
//
//        List<FloorTblDTO> firstFloor = floorTblDTOS.stream().filter(floorTblDTO -> floorTblDTO.getFloorNum() == 1).collect(Collectors.toList());
//
//        List<FloorVO> collect = firstFloor.stream().map(floorTblDTO -> {
//            List<FloorTblDTO> collect1 = floorTblDTOS.stream().filter(floorTblDTO1 -> floorTblDTO1.getParentFloorId().equals(floorTblDTO.getFloorId())).collect(Collectors.toList());
//            FloorVO floorVO = new FloorVO();
//            FloorVO.FloorList floorList = new FloorVO.FloorList();
//            BeanUtils.copyProperties(floorTblDTO,floorList);
//            floorVO.setFloorList(floorList);
//            List<FloorVO.FloorList> floorLists = new ArrayList<>();
//            List<FloorVO.FloorList> collect2 = collect1.stream().map(FloorVO.FloorList::vo).collect(Collectors.toList());
//            floorVO.setFloorLists(collect2);
//            return floorVO;
//        }).collect(Collectors.toList());
//        return ResponseResult.success(collect);
//
//    }
//}
