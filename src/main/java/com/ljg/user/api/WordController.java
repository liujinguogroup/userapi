package com.ljg.user.api;
import com.ljg.user.common.WordResult;
import com.ljg.user.service.impl.WordExportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * WORD导出
 * @author message丶小和尚
 * @create 2020/01/10
 */
@Api(value = "api/word",tags = "下载word文档")
@RestController
@RequestMapping("api/word")
public class WordController {

    @Resource
    private WordExportService wordExportService;//Word导出工具

    @ApiOperation(value = "下载word")
    @GetMapping("exportWordDemo")
    @ResponseBody
    public void exportWordDemo(HttpServletResponse response, @RequestParam("param")String param) throws Exception{
        WordResult wordResult = new WordResult();
        wordResult.setData(param);
        wordExportService.toWordDemo(response, wordResult);
    }

    @ApiOperation(value = "下载wordList")
    @GetMapping("exportWordList")
    @ResponseBody
    public void exportWordList(HttpServletResponse response, @RequestParam("param")String param) throws Exception{
        List<WordResult> wordResultList = new ArrayList<>();
        WordResult wordResult = new WordResult();
        wordResult.setData(param);
        wordResultList.add(wordResult);
        wordExportService.toWordList(response, wordResult, wordResultList);
    }
}
