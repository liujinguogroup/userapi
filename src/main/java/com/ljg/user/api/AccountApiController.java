package com.ljg.user.api;

import com.ljg.common.utils.response.ResponseResult;
import com.ljg.user.form.LoginForm;
import com.ljg.user.service.AccountService;
import com.ljg.user.service.OrderService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "")
@RestController
@RequestMapping()
public class AccountApiController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private OrderService orderService;


    @PostMapping("api/user/login")
    public ResponseResult<String> debit(@RequestBody LoginForm form){
        return accountService.login(form);
    }



}
