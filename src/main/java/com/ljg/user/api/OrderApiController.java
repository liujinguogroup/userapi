package com.ljg.user.api;

import com.ljg.common.utils.exception.CustomException;
import com.ljg.common.utils.response.ResponsePageResult;
import com.ljg.common.utils.response.ResponseResult;
import com.ljg.esserver.entities.GoodsSearch;
import com.ljg.esserver.entities.GoodsTblES;
import com.ljg.esserver.wrappers.GoodsDocWrapper;
import com.ljg.user.annotation.FrequencyVerification;
import com.ljg.user.config.Param;
import com.ljg.user.form.OrderForm;
import com.ljg.user.service.OrderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Log4j2
@RestController
@RequestMapping()
//@RefreshScope
public class OrderApiController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private GoodsDocWrapper goodsDocWrapper;

    /**
     * @RefreshScope 是@Value方式下使用的一种自动刷新配置
     * 建议使用 @ConfigurationProperties
     */
//    @Value("${param.dateformat}")
//    public String param;

    @Autowired
    private Param param;

    @FrequencyVerification
    @ApiOperation(value = "nacos配置自动拉取测试")
    @PostMapping("api/getDateFormat")
    public ResponseResult<String> getDateFormat() {
        return ResponseResult.success(LocalDateTime.now().format(DateTimeFormatter.ofPattern(param.getDateformat())));
    }


    @FrequencyVerification
    @PostMapping("api/buy")
    public ResponseResult<String> buy(@RequestBody OrderForm form) throws CustomException {

        return orderService.buy(form.transferBO());
    }

    @ApiOperation(value = "验证网关服务的nacos自动化配置")
    @GetMapping("/goods/get/{id}")
    public ResponseResult<String> get(@PathVariable("id") String id, @RequestHeader(value = "Truth",required = false) String truth) throws CustomException {
        log.info("truth:::::{},id::::{}",truth,id);
        return ResponseResult.success(truth);
    }

    @ApiOperation(value = "保存商品")
    @ApiParam(value = "参数实体", name = "model", type = "com.example.ljg.models.StorageReduceModel")
    @PostMapping("/goods/savegoods")
    public ResponseResult<Boolean> savegoods(@RequestBody GoodsTblES dto1) throws CustomException {
        goodsDocWrapper.save(dto1);
        return ResponseResult.success(true);
    }

    @ApiOperation(value = "保存商品")
    @ApiParam(value = "参数实体", name = "model", type = "")
    @PostMapping("/goods/search")
    public ResponsePageResult<GoodsTblES> search(@RequestBody GoodsSearch search) {
        return goodsDocWrapper.getGoodsDoc().search(search);
    }
}
