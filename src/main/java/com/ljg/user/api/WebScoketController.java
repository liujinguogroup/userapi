//package com.ljg.user.api;
//
//import com.alibaba.fastjson.JSON;
//import com.ljg.user.server.WebSocketServer;
//import com.ljg.user.server.WebSocketServerV2;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Controller
//public class WebScoketController
//{
//    @Autowired
//    private WebSocketServer webSocketServer;
//    @Autowired
//    private WebSocketServerV2 webSocketServerV2;
//
//    @ResponseBody
//    @RequestMapping("/sendMessage")
//    public String batchMessage(String uid,String message)
//    {
//        Map<String, String> map =new HashMap<String, String>();
//        try
//        {
//            map.put("code", "200");
//            webSocketServer.batchSendMesasge(uid,message);
//        }
//        catch (Exception e)
//        {
//            map.put("code", "-1");
//            map.put("message", e.getMessage());
//        }
//        return JSON.toJSONString(map);
//    }
//
//    @ResponseBody
//    @RequestMapping("/sendMessageV2")
//    public String batchMessageV2(String message)
//    {
//        Map<String, String> map =new HashMap<String, String>();
//        try
//        {
//            map.put("code", "200");
//            webSocketServerV2.sendInfo(message);
//        }
//        catch (Exception e)
//        {
//            map.put("code", "-1");
//            map.put("message", e.getMessage());
//        }
//        return JSON.toJSONString(map);
//    }
//
//
//    @GetMapping("/enter")
//    public String enter()
//    {
//        return "webscoketTest.html";
//    }
//}
