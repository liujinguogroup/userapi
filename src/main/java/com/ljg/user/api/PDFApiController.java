package com.ljg.user.api;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.ljg.common.utils.response.ResponseResult;
import com.ljg.user.service.PDFTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

@Api(value = "pdf生成加密",tags = "pdf生成加密" )
@RestController
@RequestMapping("api/pdf")
public class PDFApiController {

    @Autowired
    private PDFTemplateService pdfTemplateService;

    @ApiOperation(value = "下载PDF")
    @PostMapping("getpdf")
    public ResponseResult<String> getpdf(@RequestParam("password") String password) throws Exception {
        //生成电子协议
        Map<String, Object> tAgreementApplyMap = new HashMap<>();
        tAgreementApplyMap.put("name", "张三");
        tAgreementApplyMap.put("age", "12");
        tAgreementApplyMap.put("weigh", "222");


        String tAgreementElecPath = pdfTemplateService.createPdfByTemplate(tAgreementApplyMap, "src/main/resources/组合1.pdf",password);
        String savePath = String.valueOf(System.currentTimeMillis())+".pdf";
        String newPdfPath = "src/main/resources/"+(savePath);
        pdfEncrypt(tAgreementElecPath,newPdfPath,password);
        pdfTemplateService.delFile(tAgreementElecPath);
        return ResponseResult.success(newPdfPath);
    }

    @ApiOperation(value = "下载PDFV2")
    @GetMapping("getpdfV2")
    @ResponseBody
    public void getpdf(HttpServletResponse response,@RequestParam("password") String password) throws Exception {
        //生成电子协议
        Map<String, Object> tAgreementApplyMap = new HashMap<>();
        tAgreementApplyMap.put("name", "张三");
        tAgreementApplyMap.put("age", "12");
        tAgreementApplyMap.put("weigh", "222");
        pdfTemplateService.createPdfByTemplateV2(response, tAgreementApplyMap,"src/main/resources/组合1.pdf",password);

    }

    private static boolean pdfEncrypt(String filePath, String savePath, String password){
        try{
            PdfReader reader = new PdfReader(filePath);
            PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(savePath));
            // 设置密码
            stamper.setEncryption(password.getBytes(),password.getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
            stamper.close();
            reader.close();
            System.out.println("pdfEncrypt-加密成功");
            return true;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("对【{}】文件加密处理异常:{}");
            return false;
        }
    }

}
