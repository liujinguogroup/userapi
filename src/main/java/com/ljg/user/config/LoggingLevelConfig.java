package com.ljg.user.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(value = "logging.level")
public class LoggingLevelConfig {

    private String root;


}
