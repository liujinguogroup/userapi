package com.ljg.user.service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * @Author: liuJG
 * @Date: 2023-04-04 14:06
 * @Description:
 */
public interface PDFTemplateService{


    public String createPdfByTemplate(Map<String,Object> map, String templatePath,String password) throws IOException, InterruptedException;


    void createPdfByTemplateV2(HttpServletResponse response, Map<String,Object> map, String templatePath, String password) throws IOException, InterruptedException;

    void delFile(String filePath);
}
