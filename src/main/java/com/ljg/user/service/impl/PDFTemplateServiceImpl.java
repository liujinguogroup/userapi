package com.ljg.user.service.impl;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.*;
import com.ljg.user.service.PDFTemplateService;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;

/**
 * @Author: liuJG
 * @Date: 2023-04-04 14:07
 * @Description:
 */
@Service
public class PDFTemplateServiceImpl implements PDFTemplateService {


    /**
     * 根据模板创建生成pdf
     * @param map 模板中的表单数据 key-表单属性值；value-值
     * @param templatePath 模板路径
     * @return 返回生成的pdf文件路径
     */
    @Override
    public String createPdfByTemplate(Map<String,Object> map,String templatePath,String password) throws IOException, InterruptedException {
        PdfReader reader;
        ByteArrayOutputStream bos;
        PdfStamper stamper;
        String fileName = String.valueOf(System.currentTimeMillis())+".pdf";
        //生成的pdf文件存放地址 要确保文件夹的存在
        String newPdfPath = "src/main/resources/"+(fileName);
        try {
            //设置字体是必须要的，不然没法向模板pdf里写值
            BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            // 读取pdf模板
            reader = new PdfReader(templatePath);
            bos = new ByteArrayOutputStream();
            stamper = new PdfStamper(reader, bos);
            //拿到pdf模板中的表单属性
            AcroFields form = stamper.getAcroFields();
            //设置字体
            form.addSubstitutionFont(bfChinese);
            java.util.Iterator<String> it = form.getFields().keySet().iterator();
            //遍历表单属性，对每个属性赋值
            while (it.hasNext()) {
                String name = it.next().toString();
                String value = map.get(name)!=null?map.get(name).toString():null;
                System.out.println(name+"------"+value);
                form.setField(name,value);
            }
            // 如果为false那么生成的PDF文件还能编辑，一定要设为true
            stamper.setFormFlattening(false);
            stamper.close();
            Document doc = new Document();
            File file = new File(newPdfPath);
            PdfCopy copy = new PdfCopy(doc, new FileOutputStream(file));
            doc.open();
            PdfImportedPage importPage = copy.getImportedPage(new PdfReader(bos.toByteArray()), 1);
            copy.addPage(importPage);
            doc.close();
        } catch (IOException e) {
            System.out.println("生成电子协议失败，请联系管理员");
        } catch (DocumentException e) {
            System.out.println("生成电子协议失败，请联系管理员");
        }
        return newPdfPath;
    }

    @Override
    public void createPdfByTemplateV2(HttpServletResponse response, Map<String, Object> map, String templatePath, String password) throws IOException, InterruptedException {
        PdfReader reader;
        ByteArrayOutputStream bos;
        PdfStamper stamper;
        String fileName = String.valueOf(System.currentTimeMillis())+".pdf";
        //生成的pdf文件存放地址 要确保文件夹的存在
        String newPdfPath = "src/main/resources/"+(fileName);
        try {
            OutputStream outputStream = response.getOutputStream();
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/x-download");
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String userAgent = request.getHeader("User-Agent");
            byte[] byteName = userAgent.contains("MSIE") ? fileName.getBytes() : fileName.getBytes("UTF-8");//name.getBytes("UTF-8")处理safari的乱码问题
            fileName = new String(byteName, "ISO-8859-1"); // 各浏览器基本都支持ISO编码
            response.setHeader("Content-disposition", String.format("attachment; filename=\"%s\"", fileName));//文件名外的双引号处理firefox的空格截断问题

            //设置字体是必须要的，不然没法向模板pdf里写值
            BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            // 读取pdf模板
            reader = new PdfReader(templatePath);
            bos = new ByteArrayOutputStream();
            bos.writeTo(outputStream);
            stamper = new PdfStamper(reader, bos);

            //拿到pdf模板中的表单属性
            AcroFields form = stamper.getAcroFields();
            //设置字体
            form.addSubstitutionFont(bfChinese);
            java.util.Iterator<String> it = form.getFields().keySet().iterator();
            //遍历表单属性，对每个属性赋值
            while (it.hasNext()) {
                String name = it.next().toString();
                String value = map.get(name)!=null?map.get(name).toString():null;
                System.out.println(name+"------"+value);
                form.setField(name,value);
            }
            // 如果为false那么生成的PDF文件还能编辑，一定要设为true
            stamper.setFormFlattening(true);
            stamper.setEncryption(password.getBytes(),password.getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
            stamper.close();
            reader.close();
            Document doc = new Document();
            File file = new File(newPdfPath);
            PdfCopy copy = new PdfCopy(doc, new FileOutputStream(file));
            doc.open();
            PdfImportedPage importPage = copy.getImportedPage(new PdfReader(bos.toByteArray()), 1);
            copy.addPage(importPage);
            doc.close();

            byte[] bytes = bos.toByteArray();
            InputStream is = new ByteArrayInputStream(bytes);
            byte[] buff = new byte[1024];
            int len = 0;
            while ((len = is.read(buff)) != -1) {
                outputStream.write(buff, 0, len);
            }
            is.close();
            outputStream.close();
            delFile(newPdfPath);

        } catch (IOException e) {
            System.out.println("生成电子协议失败，请联系管理员");
        } catch (DocumentException e) {
            System.out.println("生成电子协议失败，请联系管理员");
        }
    }

    /**
     * 删除文件
     */
    @Override
    public void delFile(String filePath) {
        try {
            java.io.File file = new java.io.File(filePath);
            file.delete();
        } catch (Exception e) {
            System.out.println("删除文件操作出错");
            e.printStackTrace();
        }
    }

}
