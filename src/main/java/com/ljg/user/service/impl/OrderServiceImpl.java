package com.ljg.user.service.impl;

import com.ljg.common.utils.exception.CustomException;
import com.ljg.common.utils.response.ResponseResult;
//import com.ljg.goodsserver.entities.StorageReduceModel;
//import com.ljg.goodsserver.wrappers.GoodsWrapper;
import com.ljg.orderserver.entities.OrderModel;
import com.ljg.orderserver.wrappers.OrderWrapper;
import com.ljg.user.bo.GoodsBO;
import com.ljg.user.service.OrderService;
import com.ljg.userserver.entities.ReduceMoneyModel;
import com.ljg.userserver.wrappers.AccountWrapper;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderWrapper orderWrapper;
//    @Autowired
//    private GoodsWrapper goodsWrapper;
    @Autowired
    private AccountWrapper accountWrapper;

    @Override
    @GlobalTransactional
    public ResponseResult<String> buy(GoodsBO bo) throws CustomException {

        /**
         * 判断商品是否是活动商品
         * 1，秒杀
         * 2，满减
         * 3，满包邮
         * 4，满额折
         * 5，满件折
         * 6，第二件半价
         * 7，使用优惠券
         * 8，折扣
         */
//        verifyPurchaseAction(bo);

        BigDecimal total = eventFinalPrice(bo);


        OrderModel order = new OrderModel();
        BeanUtils.copyProperties(bo,order);
        order.setUserId(bo.getUserId().toString());
        order.setCommodityCode(bo.getGoodsId().toString());
        order.setMoney(200);
        order.setCount(33);
        orderWrapper.create(order);
//        StorageReduceModel reduceModel = new StorageReduceModel();
//        reduceModel.setGoodsId(bo.getGoodsId());
//        reduceModel.setCount(bo.getCount());
//        goodsWrapper.deduct(reduceModel);
        accountWrapper.reduceMoney(ReduceMoneyModel.builder()
                .money(total)
                .userId(bo.getUserId()).build());
        return ResponseResult.success();
    }

    private BigDecimal eventFinalPrice(GoodsBO bo) throws CustomException {


        return BigDecimal.valueOf(43.2);
    }


    /**
     * 基础验证
     * @param bo
     * @throws CustomException
     */
    private void verifyPurchaseAction(GoodsBO bo) throws CustomException {
        // 验证商品库存
        List<Long> goodsIds = bo.getCartBOS().stream().map(shoppingCartBO -> shoppingCartBO.getGoodsId()).collect(Collectors.toList());


    }

}
