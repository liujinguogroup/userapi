package com.ljg.user.service.impl;

import com.ljg.user.common.WordResult;
import com.ljg.user.common.utils.WordUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STVerticalJc;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * 模板下载
 */
@Slf4j
@Component
public class WordBaseService {

    private static String templateBasePath =  "src/main/resources/";
	private final static String WORD_DEMO = "word_demo.docx";
    private final static String WORD_LIST = "word_list.docx";

    //Word导出,参数配置
	public static byte[] toWordDemo(WordResult wordResult) throws Exception {
		return toWordObjectMap(wordResult,WORD_DEMO);
	}

    private static byte[] toWordObjectMap(Object obj,String filePath) throws Exception{
        byte[] bytes = null;
        if(obj != null ){
            bytes = toWordAllModelAnalysis(WordUtil.setMapObject(obj),templateBasePath + filePath);
        }
        return bytes;
    }

	private static byte[] toWordAllModelAnalysis(Map<String, Object> mapObject, String modelPath) throws Exception {
		byte[] bytes = null;
		if(mapObject != null && !StringUtils.isEmpty(modelPath)){
			File file = new File(modelPath);
			if(!file.exists()&&!file.isFile()){
				log.error("模板文件不存在");
				return null;
			}
	        try {
	        	XWPFDocument doc = WordUtil.generateWord(mapObject, modelPath);
	 	        ByteArrayOutputStream baos=new ByteArrayOutputStream();
				doc.write(baos);
                bytes =  baos.toByteArray();
				baos.close();
			} catch (Exception e) {
				log.error("文件处理异常",e);
			}
		}
		return bytes;
	}

    //Word导出,列表设置
    public static byte[] toWordList(WordResult wordResult, List<WordResult> publicResultList) throws Exception {
        return toWordObjectMapPublic(wordResult,publicResultList,WORD_LIST);
    }

    private static byte[] toWordObjectMapPublic(Object obj, List<WordResult> list, String filePath) throws Exception{
        byte[] bytes = null;
        if(obj != null ){
            bytes = toWordAllModelAnalysisPublic(WordUtil.setMapObject(obj),list,templateBasePath+filePath);
        }
        return bytes;
    }

    private static byte[] toWordAllModelAnalysisPublic(Map<String, Object> mapObject,List<WordResult> list, String modelPath) throws Exception {
        byte[] bytes = null;
        if(mapObject != null && !StringUtils.isEmpty(modelPath)){
            File file = new File(modelPath);
            if(!file.exists()&&!file.isFile()){
                log.error("模板文件不存在");
                return null;
            }
            try {
                XWPFDocument doc = WordUtil.generateWord(mapObject, modelPath);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                //插入表格
                XWPFTable table = doc.createTable();
                table.getCTTbl().addNewTblPr().addNewJc().setVal(STJc.CENTER);//设置表格居中
                //设置表头
                XWPFTableRow tableRowOne = table.getRow(0);
                XWPFParagraph paragraph0 = getStyle(doc.createParagraph(),"序号");
                tableRowOne.getCell(0).setParagraph(paragraph0);
                paragraph0.removeRun(0);
                tableRowOne.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1000));//设置宽度
                tableRowOne.getCell(0).getCTTc().addNewTcPr().addNewVAlign().setVal(STVerticalJc.CENTER);//设置水平居中
//                tableRowOne.getCell(0).getCTTc().getPList().get(0).addNewPPr().addNewJc().setVal(STJc.CENTER);
                XWPFParagraph paragraph1 = getStyle(doc.createParagraph(),"数据");
                tableRowOne.addNewTableCell().setParagraph(paragraph1);
                paragraph1.removeRun(0);
                tableRowOne.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1500));//设置宽度
                tableRowOne.getCell(1).getCTTc().addNewTcPr().addNewVAlign().setVal(STVerticalJc.CENTER);//设置水平居中
//                tableRowOne.getCell(1).getCTTc().getPList().get(0).addNewPPr().addNewJc().setVal(STJc.CENTER);
                //循环设置Word表格内容
                for (int i = 0; i < list.size(); i++) {
                    XWPFTableRow tableRow = table.createRow();
                    XWPFParagraph paragraph00 = getStyleNotBold(doc.createParagraph(),String.valueOf(i+1));
                    tableRow.getCell(0).setParagraph(paragraph00);
                    paragraph00.removeRun(0);
                    XWPFParagraph paragraph01 = getStyleNotBold(doc.createParagraph(),list.get(i).getData());
                    tableRow.getCell(1).setParagraph(paragraph01);
                    paragraph01.removeRun(0);

                    tableRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1000));
                    tableRow.getCell(0).getCTTc().addNewTcPr().addNewVAlign().setVal(STVerticalJc.CENTER);//设置水平居中
//                    tableRow.getCell(0).getCTTc().getPList().get(0).addNewPPr().addNewJc().setVal(STJc.CENTER);
                    tableRow.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1500));
                    tableRow.getCell(1).getCTTc().addNewTcPr().addNewVAlign().setVal(STVerticalJc.CENTER);//设置水平居中
//                    tableRow.getCell(1).getCTTc().getPList().get(0).addNewPPr().addNewJc().setVal(STJc.CENTER);
                }
                doc.write(byteArrayOutputStream);
                bytes = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.close();
            } catch (Exception e) {
                log.error("文件处理异常",e);
            }
        }
        return bytes;
    }

    //设置段落格式
    private static XWPFParagraph getStyle(XWPFParagraph paragraphX, String name){
        paragraphX.setAlignment(ParagraphAlignment.CENTER);//对齐方式
        XWPFRun runXOne = paragraphX.createRun();//创建文本对象
        runXOne.setText(name);
        runXOne.setFontSize(15);//字体大小
        runXOne.setFontFamily("黑体");//字体
        runXOne.setBold(true);//加粗
        runXOne.setTextPosition(10);//设置行间距
        return paragraphX;
    }

    //设置段落格式
    private static XWPFParagraph getStyleNotBold(XWPFParagraph paragraphX, String name){
        paragraphX.setAlignment(ParagraphAlignment.CENTER);//对齐方式
        XWPFRun runXOne = paragraphX.createRun();//创建文本对象
        runXOne.setText(name);
        runXOne.setFontSize(14);//字体大小
        runXOne.setBold(false);//加粗
        runXOne.setTextPosition(10);//设置行间距
        return paragraphX;
    }

}
