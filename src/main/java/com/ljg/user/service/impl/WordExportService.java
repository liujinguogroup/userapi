package com.ljg.user.service.impl;

import com.ljg.user.common.WordResult;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * 模板文档下载
 */
@Service
public class WordExportService {
	//文件名
	private final static String DEMO_WORD_FILE="模板demo文件";
    private final static String DEMO_WORD_LIST="模板list文件";

    public void toWordDemo(HttpServletResponse response, WordResult wordResult)throws Exception{
        byte[]  bytes = WordBaseService.toWordDemo(wordResult);
        if(bytes != null){
            uploadWord(response,bytes,DEMO_WORD_FILE);
        }
    }

    public void toWordList(HttpServletResponse response, WordResult wordResult, List<WordResult> publicResultList)throws Exception{
        byte[]  bytes = WordBaseService.toWordList(wordResult,publicResultList);
        if(bytes != null){
            uploadWord(response,bytes,DEMO_WORD_LIST);
        }
    }

    /**
     * 下载模板
     * @param response 响应
     * @param bytes 字节
     * @param fileName 文件名
     * @throws Exception
     */
	private void uploadWord(HttpServletResponse response, byte[] bytes, String fileName) throws Exception{
		try {
			if(bytes != null){
//              OutputStream outputStream = new FileOutputStream("E:ceshi.docx");
				OutputStream outputStream = response.getOutputStream();
				response.setCharacterEncoding("utf-8");
				response.setContentType("application/x-download");  
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
			    String userAgent = request.getHeader("User-Agent");  
			    byte[] byteName = userAgent.contains("MSIE") ? fileName.getBytes() : fileName.getBytes("UTF-8");//name.getBytes("UTF-8")处理safari的乱码问题
			    fileName = new String(byteName, "ISO-8859-1"); // 各浏览器基本都支持ISO编码  
			    response.setHeader("Content-disposition", String.format("attachment; filename=\"%s\"", fileName+".docx"));//文件名外的双引号处理firefox的空格截断问题
				InputStream is = new ByteArrayInputStream(bytes);
				byte[] buff = new byte[1024];
				int len = 0;
				while ((len = is.read(buff)) != -1) {
                    outputStream.write(buff, 0, len);
				}
				is.close();
                outputStream.close();
			}
		} catch (Exception e) {
			throw(e);
		}
	}

}
