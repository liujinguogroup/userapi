package com.ljg.user.service;

import com.ljg.user.form.LoginForm;
import com.ljg.common.utils.response.ResponseResult;

public interface AccountService {

    ResponseResult<String> login(LoginForm form);

}