package com.ljg.user.service;

import com.ljg.user.bo.GoodsBO;
import com.ljg.common.utils.exception.CustomException;
import com.ljg.common.utils.response.ResponseResult;

public interface OrderService {

    ResponseResult<String> buy(GoodsBO bo) throws CustomException;
}