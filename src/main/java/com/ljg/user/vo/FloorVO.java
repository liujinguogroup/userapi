package com.ljg.user.vo;

//import com.ljg.goodsserver.entities.FloorTblDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.List;


@Data
@ApiModel(value = "FloorVO",description = "楼层数据")
public class FloorVO {

	private FloorList floorList;

	private List<FloorList> floorLists;


	@Data
	public static class FloorList{

		@ApiModelProperty("")
		private Integer floorId;

		/**
		 * 图片
		 * */
		@ApiModelProperty("图片")
		private String image;

		/**
		 * 页面路径
		 * */
		@ApiModelProperty("页面路径")
		private String pagePath;

		/**
		 * 图片自定义宽度
		 * */
		@ApiModelProperty("图片自定义宽度")
		private Integer imageWight;

		/**
		 * 父楼层ID
		 * */
		@ApiModelProperty("父楼层ID")
		private Integer parentFloorId;

		/**
		 * 楼层名称
		 * */
		@ApiModelProperty("楼层名称")
		private String floorName;

		/**
		 * 楼层数 1表示一级楼层
		 * */
		@ApiModelProperty("楼层数 1表示一级楼层")
		private Integer floorNum;

//		public static FloorList vo(FloorTblDTO floorTblDTO) {
//			FloorList floorList = new FloorList();
//			BeanUtils.copyProperties(floorTblDTO,floorList);
//			return floorList;
//		}
	}

}