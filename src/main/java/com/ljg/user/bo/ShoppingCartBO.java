package com.ljg.user.bo;

import lombok.Data;

@Data
public class ShoppingCartBO {

    private Long goodsId;

    private Integer count;
}
