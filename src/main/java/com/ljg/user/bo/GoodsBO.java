package com.ljg.user.bo;

import lombok.Data;

import java.util.List;

/**
 * 计算下单所需参数
 */
@Data
public class GoodsBO {

    private Long userId;

    private Long id;

    private Long goodsId;

    private Integer count;

    /**
     * 模拟购物车多商品
     */
    private List<ShoppingCartBO> cartBOS;

    private List<ActivityBO> activityList;


}
