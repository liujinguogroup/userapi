package com.ljg.user.common.auth;

import com.ljg.user.annotation.FrequencyVerification;
import com.ljg.common.utils.exception.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

public class AuthenticationInterceptor implements HandlerInterceptor {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        if (handler instanceof HandlerMethod){
            FrequencyVerification annotation = ((HandlerMethod) handler).getMethod().getAnnotation(FrequencyVerification.class);
            if (annotation!=null){
                String userIp = request.getLocalAddr();
                Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent(userIp, userIp, 2, TimeUnit.SECONDS);
                if (!aBoolean){
                    throw new CustomException("操作频率太快");
                }
            }
        }
        return true;
    }
}
