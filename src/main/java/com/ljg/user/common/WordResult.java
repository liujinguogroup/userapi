package com.ljg.user.common;

import java.io.Serializable;

public class WordResult implements Serializable {

    private static final long serialVersionUID = 1L;
    public String data;

    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }
}