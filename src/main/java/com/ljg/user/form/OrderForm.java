package com.ljg.user.form;

import com.ljg.user.bo.GoodsBO;
import lombok.Data;
import org.springframework.beans.BeanUtils;


@Data
public class OrderForm {
    private Long id;
    private Long userId;
    private Long goodsId;
    private Integer count;


    public GoodsBO transferBO() {
        GoodsBO activityBO = new GoodsBO();
        BeanUtils.copyProperties(this, activityBO);
        return activityBO;
    }
}
