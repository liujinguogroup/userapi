package com.ljg.user.form;

import lombok.Data;

@Data
public class LoginForm {
    private String phone;
    private String code;
    private String password;
}
