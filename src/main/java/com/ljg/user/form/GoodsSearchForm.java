package com.ljg.user.form;

import com.ljg.common.utils.entities.PageRequest;
import lombok.Data;

@Data
public class GoodsSearchForm extends PageRequest {
    private String goodsName;
}
