#1：nacos实现服务远程配置

######1 引入依赖
````
<dependency>
<groupId>com.alibaba.cloud</groupId>
<artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
<version>2021.1</version>
</dependency>
和
<dependency>
<groupId>org.springframework.cloud</groupId>
<artifactId>spring-cloud-starter-bootstrap</artifactId>
<version>3.0.4</version>
</dependency>
````
引入spring-cloud-starter-bootstrap 是为了解决 serviceName 为空的问题，
######2 在resources下创建bootstrap.yml 将启动的服务注册到nacos中， bootstrap执行顺序优先于application.yml

## 实现远程配置自动刷新
######1：使用@Value(${a.b}) 的方式是需要在该类加@RefreshScope注解
######2：当然也可以使用 @ConfigurationProperties


# 2：启动一直报java.net.ConnectException: [NACOS HTTP-POST] The maximum number of tolerable
#######问题原因处在bootstrap.yml中为配置 
````
spring:
  cloud:
    nacos:
      config:
        server-addr: 47.94.43.230:8848
````
导致服务动态配置一直链接失败，一直重试，

# 3遇到一个问题 在common_ljg pom.xml中引入 废弃
````
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid-spring-boot-starter</artifactId>
            <version>1.2.5</version>
        </dependency>
````
当version = 1.2.6时 启动日志中seats没有注册事务，